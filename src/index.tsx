import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { reducers } from './reducers';
import './index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';

const initialState = {
  productReducers: {
    products: [
      {
        name: 'Apple iPhone 6s',
        color: 'Anthracite',
        price: '769'
      },
      {
        name: 'Samsung Galaxy S8',
        color: 'Midnight Black',
        price: '569'
      },
      {
        name: 'Huawei P9',
        color: 'Mystic Silver',
        price: '272'
      }
    ]
  }
};

const store = createStore(reducers, initialState, applyMiddleware());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
