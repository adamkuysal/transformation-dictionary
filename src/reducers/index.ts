import { combineReducers } from 'redux';
import { productReducers } from './product_reducer';
import { ProductState } from '../actions/types';

export interface StoreState {
  productReducers: ProductState;
}

export const reducers = combineReducers<StoreState>({
  productReducers
});
