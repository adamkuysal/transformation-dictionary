import { ProductState, ActionTypes, ProductActionTypes } from '../actions/types';

const initialState: ProductState = {
  products: []
};

export const productReducers = (state = initialState, action: ProductActionTypes): ProductState => {
  switch (action.type) {
    case ActionTypes.CREATE_PRODUCT:
      return {
        ...state,
        products: [...state.products, action.payload]
      };
    case ActionTypes.DELETE_PRODUCT:
      console.log(action);
      return {
        ...state,
        products: state.products.filter(product => product.name !== action.payload.name)
      };
    case ActionTypes.UPDATE_COLOR:
      const updatedData = state.products.map(item => {
        if (item.name === action.payload.name) {
          return { ...item, color: action.payload.color };
        } else {
          return item;
        }
      });
      return {
        ...state,
        products: updatedData
      };

    default:
      return state;
  }
};
