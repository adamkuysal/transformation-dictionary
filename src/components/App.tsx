import React, { FunctionComponent } from 'react';
import Header from './Header';
import Products from './Products/ProductsConnect';
import './App.css';

const App: FunctionComponent<{}> = () => {
  return (
    <div className="App">
      <Header />
      <main>
        <Products />
      </main>
    </div>
  );
};

export default App;
