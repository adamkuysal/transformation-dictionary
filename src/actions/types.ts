export interface Product {
  name: string;
  color: string;
  price: string;
}

export interface ProductState {
  products: Product[];
}

export enum ActionTypes {
  CREATE_PRODUCT = 'CREATE_PRODUCT',
  DELETE_PRODUCT = 'DELETE_PRODUCT',
  UPDATE_COLOR = 'UPDATE_COLOR'
}

interface CreateProductAction {
  type: typeof ActionTypes.CREATE_PRODUCT;
  payload: Product;
}

interface DeleteProductAction {
  type: typeof ActionTypes.DELETE_PRODUCT;
  payload: Product;
}

interface UpdateColorAction {
  type: typeof ActionTypes.UPDATE_COLOR;
  payload: Product;
}

export type ProductActionTypes = CreateProductAction | DeleteProductAction | UpdateColorAction;
