import React, { FunctionComponent, ChangeEvent } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';
import { Product } from '../../actions/types';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: 650,
      marginTop: '30px',
      overflowX: 'auto',
      margin: 'auto'
    },
    table: {
      minWidth: 650,
      backgroundColor: '#f9f9ff'
    },
    tableCell: {
      textTransform: 'capitalize'
    },
    textField: {
      width: 150
    }
  });

interface ListProductProps {
  classes: any;
  products: Product[];
  deleteProduct: (data: Product) => void;
  makeEditable: (name: string) => void;
  transformColor: (product: Product) => void;
  enableEdit: boolean;
  selectedProduct: string;
  handleColorChange: (color: string) => void;
}

const ListProduct: FunctionComponent<ListProductProps> = ({
  classes,
  products,
  deleteProduct,
  makeEditable,
  transformColor,
  enableEdit,
  selectedProduct,
  handleColorChange
}) => {
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell align="center"></TableCell>
            <TableCell align="center">Product Name</TableCell>
            <TableCell align="center">Color</TableCell>
            <TableCell align="center">Price</TableCell>
            <TableCell align="center"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {products &&
            products.map((product: Product) => (
              <TableRow key={product.name}>
                <TableCell align="center">
                  {enableEdit && selectedProduct === product.name ? (
                    <SaveIcon onClick={(): void => transformColor(product)} />
                  ) : (
                    <EditIcon onClick={(): void => makeEditable(product.name)} />
                  )}
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  {product.name}
                </TableCell>
                {enableEdit && selectedProduct === product.name ? (
                  <TableCell className={classes.tableCell} align="center">
                    <TextField
                      defaultValue={product.color}
                      margin="normal"
                      id="outlined-bare"
                      className={classes.textField}
                      onChange={(e: ChangeEvent<HTMLInputElement>): void => handleColorChange(e.target.value)}
                      variant="outlined"
                      inputProps={{ 'aria-label': 'bare' }}
                    />
                  </TableCell>
                ) : (
                  <TableCell className={classes.tableCell} align="center">
                    {product.color}
                  </TableCell>
                )}
                <TableCell align="center">CHF {product.price}</TableCell>
                <TableCell align="center">
                  <DeleteIcon onClick={(): void => deleteProduct(product)} />
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default withStyles(styles)(ListProduct);
