import React, { FunctionComponent, ChangeEvent, FocusEvent } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Product } from '../../actions/types';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3, 2),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingTop: '10px',
      minWidth: '400px',
      margin: '30px 0px 30px 40px',
      backgroundColor: '#f9f9ff'
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative'
    },
    textField: {
      marginLeft: '16px',
      marginRight: '16px',
      width: '300px',
      display: 'flex'
    },
    dense: {
      marginTop: 16
    },
    menu: {
      width: 200
    },
    buttonContainer: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '28px'
    },
    button: {
      height: '50px',
      width: '146px',
      fontSize: '0.8125rem'
    }
  });

export interface CreateProjectProps {
  classes: any;
  handleChange: (e: string, name: string) => void;
  handleBlur: (e: string, name: string) => void;
  items: any;
  itemsValidation: any;
  submitForm: () => void;
}

const CreateProject: FunctionComponent<CreateProjectProps> = props => {
  const { classes, handleChange, handleBlur, items, itemsValidation, submitForm } = props;

  const titles = [
    { id: 'name', label: 'Product Name' },
    { id: 'color', label: 'Color' },
    { id: 'price', label: 'Price' }
  ];

  const validated = items && Object.values(items).every(el => el !== '');

  return (
    <div>
      <Paper className={classes.root}>
        <Typography variant="h6" color="inherit" style={{ marginBottom: '10px' }}>
          Create New Product
        </Typography>
        <div className={classes.container}>
          {titles.map(title => (
            <TextField
              error={itemsValidation[title.id]}
              key={title.id}
              id={title.id}
              label={title.label}
              className={classes.textField}
              value={items[title.id]}
              onChange={(e: ChangeEvent<HTMLInputElement>): void => handleChange(e.target.value, title.id)}
              onBlur={(e: FocusEvent<HTMLInputElement>): void => handleBlur(e.target.value, title.id)}
              margin="normal"
              variant="outlined"
            />
          ))}
        </div>
        <div className={classes.buttonContainer}>
          <Button
            onClick={(): void => {
              submitForm();
            }}
            variant="contained"
            color="primary"
            className={classes.button}
            disabled={!validated}
          >
            Submit
          </Button>
        </div>
      </Paper>
    </div>
  );
};

export default withStyles(styles)(CreateProject);
