import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Products from './Products';
import { StoreState } from '../../reducers';
import { createProduct, deleteProduct, updateColor } from '../../actions';
import { ProductActionTypes, Product } from '../../actions/types';

const mapStateToProps = (state: StoreState) => ({
  productReducers: state.productReducers
});

const mapDispatchToProps = (dispatch: Dispatch<ProductActionTypes>) => ({
  createProduct: (data: Product) => dispatch(createProduct(data)),
  deleteProduct: (data: Product) => dispatch(deleteProduct(data)),
  updateColor: (data: Product) => dispatch(updateColor(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products);
