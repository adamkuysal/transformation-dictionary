import React from 'react';

const Header = () => {
  return (
    <header className="StickyHeader">
      <span>Frontend Assesment Test</span>
    </header>
  );
};

export default Header;
