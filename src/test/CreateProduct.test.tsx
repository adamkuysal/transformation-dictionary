import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import CreateProduct, { CreateProjectProps } from '../components/Products/CreateProduct';

describe('<CreateProduct />', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const renderComponent = (props: Partial<CreateProjectProps> = {}) => {
    const defaultProps: CreateProjectProps = {
      handleChange() {
        return;
      },
      handleBlur() {
        return;
      },
      submitForm() {
        return;
      },
      items: {},
      itemsValidation: {},
      classes: {}
    };
    return render(<CreateProduct {...defaultProps} {...props} />);
  };

  it('renders component', () => {
    renderComponent();
  });

  it('selects Submit button', () => {
    const submitForm = jest.fn();
    const { getByText, getByLabelText } = renderComponent({ submitForm });

    fireEvent.click(getByText('Submit'));
    expect(submitForm).toHaveBeenCalledTimes(1);
  });

  it('shows form title correctly', () => {
    const { getByText } = renderComponent();

    expect(getByText('Create New Product')).toBeInTheDocument();
  });

  it('calls handleChange function with correct argument', () => {
    const handleChange = jest.fn();
    const { getByLabelText } = renderComponent({ handleChange });
    const textField = getByLabelText('Product Name');

    fireEvent.change(textField, { target: { value: 'Samsung XXX' } });
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith('Samsung XXX', 'name');
  });
});
