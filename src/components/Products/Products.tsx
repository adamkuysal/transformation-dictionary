import React, { Component } from 'react';
import { ProductState, Product } from '../../actions/types';
import CreateProduct from './CreateProduct';
import ListProduct from './ListProduct';

export interface ProductsProps {
  productReducers: ProductState;
  createProduct: (product: Product) => void;
  deleteProduct: (product: Product) => void;
  updateColor: (product: Product) => void;
}

class Products extends Component<ProductsProps> {
  state = {
    enableEdit: false,
    selectedProduct: '',
    transformedColor: '',
    items: {
      name: '',
      color: '',
      price: ''
    },
    itemsValidation: {
      name: false,
      color: false,
      price: false
    }
  };

  handleChange = (value: string, name: string) => {
    this.setState({
      items: { ...this.state.items, [name]: value }
    });
  };

  handleBlur = (value: string, name: string) => {
    if (value === '') {
      this.setState({ itemsValidation: { ...this.state.itemsValidation, [name]: true } });
    } else {
      this.setState({ itemsValidation: { ...this.state.itemsValidation, [name]: false } });
    }
  };

  handleColorChange = (value: string) => {
    this.setState({
      transformedColor: value
    });
  };

  makeEditable = (name: string): void => {
    this.setState({ enableEdit: true, selectedProduct: name });
  };

  transformColor = (product: Product): void => {
    this.props.updateColor({ name: product.name, color: this.state.transformedColor, price: product.price });
    this.setState({ enableEdit: false, selectedProduct: '' });
  };

  submitForm = (): void => {
    const { items }: { items: any } = this.state;

    const validated = items && Object.values(items).every(el => el !== '');

    if (validated) {
      const trimmedArry = Object.keys(items).map((k: string) => {
        return { [k]: items[k].trim() };
      });
      const trimmedObj = Object.assign({}, ...trimmedArry);

      console.log(trimmedObj);

      this.props.createProduct(trimmedObj);
      this.setState({
        items: { name: '', color: '', price: '' }
      });
    }
  };

  render() {
    const { productReducers, deleteProduct } = this.props;
    const { enableEdit, selectedProduct, items, itemsValidation } = this.state;

    return (
      <div className="ProductsContainer">
        <CreateProduct
          handleChange={this.handleChange}
          handleBlur={this.handleBlur}
          items={items}
          itemsValidation={itemsValidation}
          submitForm={this.submitForm}
        />
        <ListProduct
          products={productReducers.products}
          deleteProduct={deleteProduct}
          makeEditable={this.makeEditable}
          transformColor={this.transformColor}
          enableEdit={enableEdit}
          selectedProduct={selectedProduct}
          handleColorChange={this.handleColorChange}
        />
      </div>
    );
  }
}

export default Products;
