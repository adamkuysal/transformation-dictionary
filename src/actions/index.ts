import { Product, ActionTypes } from './types';

export const createProduct = (product: Product) => ({
  type: ActionTypes.CREATE_PRODUCT,
  payload: product
});

export const deleteProduct = (product: Product) => ({
  type: ActionTypes.DELETE_PRODUCT,
  payload: product
});

export const updateColor = (product: Product) => ({
  type: ActionTypes.UPDATE_COLOR,
  payload: product
});
